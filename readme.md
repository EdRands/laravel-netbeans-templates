#Laravel File and Code Templates for NetBeans IDE
## Get your laravel coding done faster in NetBeans

I created these for my own use for the way I work. You may need to modify them for your own use.

I put them here for the benefit of others, and so I can find them if I move to another computer.

Please note that the code is not valid php/html/etc the way they are.
They contain FreeMarker code so NetBeans will intelligently fill things out based on input.

### Code Templates
The \code directory contains code templates.
Add them individually in Tools->Options->Editor->Code Templates

### File Templates
The \files directory contains templates for entire files
